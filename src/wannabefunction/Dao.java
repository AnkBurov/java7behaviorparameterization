package wannabefunction;

import java.util.concurrent.Callable;

public class Dao implements KindOfFunctionalInterface {

    public int return1() {
        return 1;
    }

    public int return2() {
        return 2;
    }


    @Override
    public <T> T apply(Callable<T> func) throws Exception {
        return func.call();
    }
}
