package wannabefunction;

import java.util.concurrent.Callable;

public interface KindOfFunctionalInterface {

    <T> T apply(Callable<T> func) throws Exception;
}
