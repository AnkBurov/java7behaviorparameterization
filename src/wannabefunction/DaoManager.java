package wannabefunction;

import java.util.concurrent.Callable;

public class DaoManager {
    Dao dao = new Dao();

    private <T> T returnFromDao(final Callable<T> callable) throws Exception {
        return dao.apply(callable);
    }

    public int return1() throws Exception {
        // вызываем метод напрямую, но можно через тот же select в абстрактном дао менеджере
        try {
            return returnFromDao(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return dao.return1();
                }
            });
        } catch (Exception e) {
            // Пример обработки исключения
            e.printStackTrace();
            throw e;
        }
    }

    public int return2() throws Exception {
        // вызываем метод напрямую, но можно через тот же select в абстрактном дао менеджере
        try {
            return returnFromDao(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return dao.return2();
                }
            });
        } catch (Exception e) {
            // Пример обработки исключения
            e.printStackTrace();
            throw e;
        }
    }
}
